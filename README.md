###OR-Manager
###Description
This basic Python WebAPI back-end sample implements a basic Operation Room Admission.The back-end designed by Python( Quart microframework) and Database is MySQL
###Requirements
###### - Python link : [https://www.python.org/downloads/](https://www.python.org/downloads/)
###### - MySQL link : [https://www.mysql.com/downloads/](https://www.mysql.com/downloads/)
###### - Visual Studio Code link : [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
###Setup
- Install Python
	- import libraly : quart , mysql-connector , jsonschema
		- Terminal: pip install quart mysql-connector jsonschema
- Install MySQL
- Install Visual Studio Code
	- Import extendsion : python , sql , html , etc..
- In terminal setup environment for api service
	- Example
		- Mycom: export DB_USER= MySQL_User
		- Mycom: export DB_HOST= localhost
		- Mycom: export DB_PASSWORD= MySQL_Password
		- Mycom: export DB_DATABASE=or_manager
		- Mycom: export QUART_APP=manage.py
- In terminal test MySQL shell
			Mycom: mysql -u MySQL_User -p
			password: ********** (Enter MySQL Password)
			- Created database name is : or_manager
			- Created table name is : manager see link [table.sql](https://bitbucket.org/pinyoo/or_manager/src/master/table.sql)
- In terminal you folder project
	- you can run api: quart run
	- URL :  http://localhost:5000/managers/

###Good URL Example
	- Get all manager
		- GET: http://localhost:5000/managers/
	- Get one manager by id
		- GET: http://localhost:5000/managers/1/
	- Get Filter by room
		- GET: http://localhost:5000/managers/?room=name room
	- Get Filter by zone
		- GET: http://localhost:5000/managers/?zone=name zone
	- Get Filter by ward
		- GET: http://localhost:5000/managers/?ward=name ward
	- Get Filter by surgeon
		- GET: http://localhost:5000/managers/?surgeon=name surgeon
	- Get Filter by admit
		- GET: http://localhost:5000/managers/?admit=name admit
	- Get Filter by patient HN
		- GET: http://localhost:5000/managers/?hn=patient HN
	- Get Filter by patient name
		- GET: http://localhost:5000/managers/?name=patient name
	- Get Filter by diagnosis
		- GET: http://localhost:5000/managers/?diagnosis=diagnosis
	- Get Filter by operation
		- GET: http://localhost:5000/managers/?operation=operation
	- Get Filter by anest
		- GET: http://localhost:5000/managers/?anest=YES or NO
	- Get Filter by register by
		- GET: http://localhost:5000/managers/?by=name register
	- Get Filter by date register
		- GET: http://localhost:5000/managers/?date=date register
	- Get Filter by online case
		- GET: http://localhost:5000/managers/?online=online case
	- Post new manager
		- POST: http://localhost:5000/managers/
		- Example
		{
			"room":   "OR410",
			"zone":   "CVT",
			"start":  "2018-08-26T00:00:00Z",
			"ward":   "84/4 ตก",
			"surgeon":"อ.อำนวย",
			"admit":  "Pre-Op",
			"patient_hn": "53761038",
			"patient_name": "นางอำนาจ อาจนรง",
			"patient_age": 60,
			"patient_sex": "FEMALE",
			"patient_w": 50.5,
			"patient_h": 162,
			"diagnosis": "oa knee",
			"operation": "bilat tka",
			"anest": "YES",
			"regisby": "นพ.ออเจ้า",
			"createdAt": "2018-08-26T00:00:00Z",
			"online_case": "กำลังผ่าตัด"
	}
	- Update when end case
		- PUT: http://localhost:5000/managers/1/
		- Example
			{
				"end":  "2018-08-26T06:30:10Z",
				"online_case": "ผ่าตัดเสร็จแล้ว"
			}
	- Delete one manager
		- DELETE: http://localhost:5000/managers/1/

###Responses
- Response in JSON
###Request & Response Examples
####API Resources
- GET /managers/
- GET /managers/[id]/
- POST /managers/
- PUT /managers/[id]
- DELETE /managers/[id]
### GET /managers/
		- Example : http://localhost:5000/managers/
		- Response body:
		{
    		"msg": "ok",
    		"result": [
        		{
            		"admit": "Pre-Op",
            		"anest": "YES",
            		"createdAt": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"diagnosis": "oa knee",
            		"end": "Sat, 25 Aug 2018 23:30:10 GMT",
            		"id": 1,
            		"online_case": "ผ่าตัดเสร็จแล้ว",
            		"operation": "bilat tka",
            		"patient_age": 60,
            		"patient_h": 162,
            		"patient_hn": "53761038",
            		"patient_name": "นางอำนาจ อาจนรง",
            		"patient_sex": "FEMALE",
            		"patient_w": 50.5,
            		"regisby": "นพ.ออเจ้า",
            		"room": "OR409",
            		"start": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"surgeon": "อ.อำนวย",
            		"ward": "84/4 ตก",
            		"zone": "CVT"
        		},
		  		{
            		"admit": "Pre-Op",
            		"anest": "YES",
            		"createdAt": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"diagnosis": "oa knee",
            		"end": "Sat, 25 Aug 2018 23:30:10 GMT",
            		"id": 2,
            		"online_case": "ผ่าตัดเสร็จแล้ว",
            		"operation": "bilat tka",
            		"patient_age": 60,
            		"patient_h": 162,
            		"patient_hn": "53761038",
           		    "patient_name": "นางอำนาจ อาจนรง",
            		"patient_sex": "FEMALE",
            		"patient_w": 50.5,
            		"regisby": "นพ.ออเจ้า",
            		"room": "OR410",
            		"start": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"surgeon": "อ.อำนวย",
            		"ward": "84/4 ตก",
            		"zone": "CVT"
        		}
			]
		}
### GET /managers/[id]
		- Example : http://localhost:5000/managers/2/
		- Response body:
		{
    		"msg": "ok",
    		"result": [
        		{
            		"admit": "Pre-Op",
            		"anest": "YES",
            		"createdAt": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"diagnosis": "oa knee",
            		"end": "Sat, 25 Aug 2018 23:30:10 GMT",
            		"id": 2,
            		"online_case": "ผ่าตัดเสร็จแล้ว",
            		"operation": "bilat tka",
            		"patient_age": 60,
            		"patient_h": 162,
            		"patient_hn": "53761038",
            		"patient_name": "นางอำนาจ อาจนรง",
            		"patient_sex": "FEMALE",
            		"patient_w": 50.5,
            		"regisby": "นพ.ออเจ้า",
            		"room": "OR410",
            		"start": "Sat, 25 Aug 2018 17:00:00 GMT",
            		"surgeon": "อ.อำนวย",
            		"ward": "84/4 ตก",
            		"zone": "CVT"
        		}
    		]
		}
### POST /managers/
		- Example : http://localhost:5000/managers/
		- Request body JSON
				{
					"room":   "OR410",
					"zone":   "CVT",
					"start":  "2018-08-26T00:00:00Z",
					"ward":   "84/4 ตก",
					"surgeon":"อ.อำนวย",
					"admit":  "Pre-Op",
					"patient_hn": "53761038",
					"patient_name": "นางอำนาจ อาจนรง",
					"patient_age": 60,
					"patient_sex": "FEMALE",
					"patient_w": 50.5,
					"patient_h": 162,
					"diagnosis": "oa knee",
					"operation": "bilat tka",
					"anest": "YES",
					"regisby": "นพ.ออเจ้า",
					"createdAt": "2018-08-26T00:00:00Z",
					"online_case": "กำลังผ่าตัด"
				}
		- Response body
				{
    				"msg": "Save successed"
				}


### PUT /managers/[id]
		- Example : http://localhost:5000/managers/2/
		- Request body JSON
				{
	 				"end":  "2018-08-26T06:30:10Z",
	 				"online_case": "ผ่าตัดเสร็จแล้ว"
				}
		- Response body
				{
    				"msg": "Update successed"
				}
### DELETE /managers/[id]
		- Example : http://localhost:5000/managers/1/
		- Response body
				{
    				"msg": "Delete successed"
				}

