from quart.views import MethodView
from quart import Quart, jsonify, request, abort

from jsonschema import Draft4Validator
from jsonschema.exceptions import best_match

import datetime
import re
import math

from database import database
from managers.schema import schema,schema1
from managers.templates import dataset_obj, datasets_obj

class ManagerAPI(MethodView):

    def __init__(self):

        if (request.method != 'GET' and request.method != 'DELETE') and not request.get_json:
            abort(400)
    
    async def get(self,manager_id):
        sql = ""

        # Filter by room OR
        if not request.args.get('room'):
            room = ""
        else:
            room = request.args.get('room')

            if regx(room,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Room error'}), 400
            else:
                sql += " AND room LIKE '%s'"%("%"+str(room)+"%")

        # Filter by Zone OR
        if not request.args.get('zone'):
            zone = ""
        else:
            zone = request.args.get('zone')

            if regx(zone,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Zone error'}), 400
            else:
                sql += " AND zone LIKE '%s'"%("%"+str(zone)+"%")
        
        # Filter by ward
        if not request.args.get('ward'):
            ward = ""
        else:
            ward = request.args.get('ward')

            if regx(ward,r'[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Ward error'}), 400
            else:
                sql += " AND ward LIKE '%s'"%("%"+str(ward)+"%")
        
        # Filter by surgeon
        if not request.args.get('surgeon'):
            surgeon = ""
        else:
            surgeon = request.args.get('surgeon')

            if regx(surgeon,r'[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Surgeon error'}), 400
            else:
                sql += " AND surgeon LIKE '%s'"%("%"+str(surgeon)+"%")
        
        # Filter by admit
        if not request.args.get('admit'):
            admit = ""
        else:
            admit = request.args.get('admit')

            if regx(admit,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Admit error'}), 400
            else:
                sql += " AND admit LIKE '%s'"%("%"+str(admit)+"%")
        
        # Filter by HN
        if not request.args.get('hn'):
            hn = ""
        else:
            hn = request.args.get('hn')

            if regx(hn,r'a-zA-Zก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'HN error'}), 400
            else:
                sql += " AND patient_hn LIKE '%s'"%("%"+str(hn)+"%")
        
        # Filter by Patient Name
        if not request.args.get('name'):
            pn = ""
        else:
            pn = request.args.get('name')

            if regx(pn,r'[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Patient name error'}), 400
            else:
                sql += " AND patient_name LIKE '%s'"%("%"+str(pn)+"%")
        
        # Filter by diagnosis
        if not request.args.get('diagnosis'):
            diag = ""
        else:
            diag = request.args.get('diagnosis')

            if regx(diag,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Diagnosis error'}), 400
            else:
                sql += " AND diagnosis LIKE '%s'"%("%"+str(diag)+"%")
        
        # Filter by operation
        if not request.args.get('operation'):
            operation = ""
        else:
            operation = request.args.get('operation')

            if regx(operation,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Operation error'}), 400
            else:
                sql += " AND operation LIKE '%s'"%("%"+str(operation)+"%")
        
        # Filter by anest
        if not request.args.get('anest'):
            anest = ""
        else:
            anest = request.args.get('anest')

            if regx(anest,r'ก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Anest error'}), 400
            else:
                sql += " AND anest LIKE '%s'"%("%"+str(anest)+"%")
        
        # Filter by Register user
        if not request.args.get('by'):
            by = ""
        else:
            by = request.args.get('by')

            if regx(by,r'[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Register by error'}), 400
            else:
                sql += " AND regisby LIKE '%s'"%("%"+str(by)+"%")
        
        # Filter by created At
        if not request.args.get('date'):
            date = ""
        else:
            date = request.args.get('date')

            if regx(date,r'a-zA-Zก-๙[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Date error'}), 400
            else:
                sql += " AND createdAt LIKE '%s'"%("%"+str(date)+"%")
        
        # Filter by Online case
        if not request.args.get('online'):
            online = ""
        else:
            online = request.args.get('online')

            if regx(online,r'[}*"|/><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Online case error'}), 400
            else:
                sql += " AND online_case LIKE '%s'"%("%"+str(online)+"%")
        
        # Filter by Id
        if manager_id:
            if regx(str(manager_id),r'[a-zA-Zก-๙[}*"|./><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Id is not int'}), 400
            else:
                sql += " AND id='%d'"%int(manager_id)
                
        query = "SELECT * FROM manager WHERE 1%s AND online=1 ORDER BY id ASC"%sql

        db = database()
        cursor = await db.Query(query)

        if cursor:
            response = {
                'msg': 'ok',
                'result': await datasets_obj(cursor)
            }
            return jsonify(response), 200
        else:
            response = {
                'msg': 'No data'
            }
            return jsonify(response), 404
    
    async def post(self,manager_id):
        manager_json = await request.get_json()
        error = best_match(Draft4Validator(schema).iter_errors(manager_json))

        if error:
            return jsonify({"error": error.message}), 400
        else:
            try:
                register_date = datetime.datetime.strptime(manager_json.get('createdAt'),"%Y-%m-%dT%H:%M:%SZ")
                start_case    = datetime.datetime.strptime(manager_json.get('start'),"%Y-%m-%dT%H:%M:%SZ")
            except:
                return jsonify({"error":"INVALID_DATE"}), 400
            
            sql = """
                        INSERT INTO manager(
                            room,
                            zone,
                            start,
                            ward,
                            surgeon,
                            admit,
                            patient_hn,
                            patient_name,
                            patient_age,
                            patient_sex,
                            patient_w,
                            patient_h,
                            diagnosis,
                            operation,
                            anest,
                            regisby,
                            createdAt,
                            online_case,
                            online
                        )
                        VALUES(
                            '%s','%s','%s','%s','%s',
                            '%s','%s','%s','%d','%s',
                            '%f','%f','%s','%s','%s',
                            '%s','%s','%s','%i'
                        )
                  """%(
                      str(manager_json.get('room')),
                      str(manager_json.get('zone')),
                      str(start_case),
                      str(manager_json.get('ward')),
                      str(manager_json.get('surgeon')),
                      str(manager_json.get('admit')),
                      str(manager_json.get('patient_hn')),
                      str(manager_json.get('patient_name')),
                      int(manager_json.get('patient_age')),
                      str(manager_json.get('patient_sex')),
                      float(manager_json.get('patient_w')),
                      float(manager_json.get('patient_h')),
                      str(manager_json.get('diagnosis')),
                      str(manager_json.get('operation')),
                      str(manager_json.get('anest')),
                      str(manager_json.get('regisby')),
                      str(register_date),
                      str(manager_json.get('online_case')),
                      True
                  )
            db = database()

            cursor = await db.InsertOne(sql)

            if cursor == True:
                response = {
                    'msg': 'Save successed'
                }
                return jsonify(response), 201
            else:
                response = {
                    'msg': 'Save unsuccessed'
                }
                return jsonify(response), 404

    async def put(self,manager_id):
        if manager_id:
            if regx(str(manager_id),r'[a-zA-Zก-๙[}*"|./><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Id is not int'}), 400
            else:
                manager_json = await request.get_json()
                error = best_match(Draft4Validator(schema1).iter_errors(manager_json))

                if error:
                    return jsonify({"error": error.message}), 400
                else:
                    try:
                        end_case    = datetime.datetime.strptime(manager_json.get('end'),"%Y-%m-%dT%H:%M:%SZ")
                    except:
                        return jsonify({"error":"INVALID_DATE"}), 400
                    
                    online = False

                    sql = """UPDATE manager 
                             SET end='%s',online_case='%s' 
                             WHERE id=%d"""%(
                                 str(end_case),
                                 str(manager_json.get('online_case')),
                                 int(manager_id)
                            )
                
                    db = database()
                    cursor = await db.UpdateOne(sql,manager_id)

                    if cursor==True:
                        response = {
                            'msg': 'Update successed'
                        }
                        return jsonify(response), 200
                    else:
                        response = {
                            'msg': 'Update unsuccessed'
                        }
                        return jsonify(response), 404
    
    async def delete(self,manager_id:int):
        if manager_id:
            if regx(str(manager_id),r'[a-zA-Zก-๙[}*"|./><{;=?#_+%@$)(]')==False:
                return jsonify({'msg':'Id is not int'}), 400
            else:
                
                online = False

                sql = "UPDATE manager SET online=%i WHERE id=%d"%(online,int(manager_id))
                
                db = database()
                cursor = await db.DeleteOne(sql,manager_id)

                if cursor==True:
                    response = {
                        'msg': 'Delete successed'
                    }
                    return jsonify(response), 200
                else:
                    response = {
                        'msg': 'Delete unsuccessed'
                    }
                    return jsonify(response), 404

def regx(string:str,param):
    if re.search(param,string):
        return False
    else:
        return True