import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from application import create_app

app = create_app()

if __name__ == "__main__":
    port = 5000
    host = '0.0.0.0'
    debug = True
    app.run(
        debug = debug,
        port  = port,
        host  = host
    )
