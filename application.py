
from quart import Quart 

def create_app(**config_overrides):

    # Initial Qaurt
    app = Quart(__name__)

    app.config.update(config_overrides)

    from managers.views import manager_app

    app.register_blueprint(manager_app)

    return app
    

