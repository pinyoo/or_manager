from __future__ import print_function
import os
import mysql.connector
from datetime import date,datetime,timedelta

class database:

    def __init__(self):
        self.conn = mysql.connector.connect(
                host = os.getenv('DB_HOST'),
                user = os.getenv('DB_USER'),
                passwd = os.getenv('DB_PASSWORD'),
                database = os.getenv('DB_DATABASE')
        )

    async def Query(self,sql):
        
        try:
            conn = self.conn
            cursor = conn.cursor()
            cursor.execute(sql)
            result = cursor.fetchall()
        except mysql.connector.Error as e:
            cursor.close()
            conn.close()
        finally:
            cursor.close()
            conn.close()
            return result
    
    async def InsertOne(self,sql):
        
        try:
            conn = self.conn
            cursor = conn.cursor()
            cursor.execute(sql)
            conn.commit()
        except mysql.connector.Error as e:
            cursor.close()
            conn.close()
            return False
        finally:
            cursor.close()
            conn.close()
            return True
    
    async def DeleteOne(self,sql,id):
        try:
            value = True
            conn = self.conn
            cursor = conn.cursor()
            query = "SELECT * FROM manager WHERE online=1 AND id=%d"%int(id)
            cursor.execute(query)
            row = cursor.fetchall()
            if row:
                cursor.execute(sql)
                conn.commit()
                value = True
            else:
                value = False
        except mysql.connector.Error as e:
            cursor.close()
            conn.close()
            return False
        finally:
            cursor.close()
            conn.close()
            return value
    
    async def UpdateOne(self,sql,id):
        try:
            value = True
            conn = self.conn
            cursor = conn.cursor()
            query = "SELECT * FROM manager WHERE end IS NULL AND online=1 AND id=%d"%int(id)
            cursor.execute(query)
            row = cursor.fetchall()
            if row:
                print(row)
                cursor.execute(sql)
                conn.commit()
                value = True
            else:
                value = False
        except mysql.connector.Error as e:
            cursor.close()
            conn.close()
            return False
        finally:
            cursor.close()
            conn.close()
            return value



