def dataset_obj(x):
     return {
                'id': x[0],
                'room': x[1],
                'zone': x[2],
                'start': x[3],
                'end': x[4],
                'ward': x[5],
                'surgeon': x[6],
                'admit': x[7],
                'patient_hn': x[8],
                'patient_name': x[9],
                'patient_age': x[10],
                'patient_sex': x[11],
                'patient_w': x[12],
                'patient_h': x[13],
                'diagnosis': x[14],
                'operation': x[15],
                'anest': x[16],
                'regisby': x[17],
                'createdAt': x[18],
                'online_case': x[19]
            }

async def datasets_obj(objs):
    datasets_obj = list(map(dataset_obj,objs))
    return datasets_obj