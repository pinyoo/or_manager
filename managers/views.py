from quart import Blueprint

from managers.api import ManagerAPI

manager_app = Blueprint('manager_app',__name__)

manager_view = ManagerAPI.as_view('manager_api')

manager_app.add_url_rule('/managers/<manager_id>/',view_func=manager_view,
                        methods=['GET','PUT','DELETE',])

manager_app.add_url_rule('/managers/',defaults={'manager_id': None},
                    view_func =manager_view, methods=['GET','POST'])

