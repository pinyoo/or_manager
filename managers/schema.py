schema = {
    "type": "object",
    "properties": {
        "room":         { "type": "string","pattern": "^[a-zA-Zก-๙0-9]" },     
        "zone":         { "type": "string","pattern": "^[a-zA-Z]" },     
        "start":        { "type": "string","pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$" },
        "ward":         { "type": "string","pattern": "^[a-zA-Z0-9ก-๙]"},
        "surgeon":      { "type": "string","pattern": "^[ก-๙a-zA-Z]"},
        "admit":        { "type": "string","pattern": "^[a-zA-Z_-]"},
        "patient_hn":   { "type": "string","pattern": "^[0-9]"},
        "patient_name": { "type": "string","pattern": "^[a-zA-Zก-๙]"},
        "patient_age":  { "type": "integer"},
        "patient_sex":  { "type": "string","pattern": "^[A-Z]"},
        "patient_w":    { "type": "number"},
        "patient_h":    { "type": "number"},
        "diagnosis":    { "type": "string","pattern": "^[a-zA-Z0-9]"},
        "operation":    { "type": "string","pattern": "^[a-zA-Z0-9]"},
        "anest":        { "type": "string","pattern": "^[A-Z]"},
        "regisby":      { "type": "string","pattern": "^[a-zA-Zก-๙]"},
        "createdAt":    { "type": "string","pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$" },
        "online_case":  { "type": "string","pattern": "^[a-zA-Zก-๙]"},      
    },
    "required": [
        "room",     
        "zone",     
        "start",
        "ward",
        "surgeon",
        "admit",
        "patient_hn",
        "patient_name",
        "patient_age",
        "patient_sex",
        "patient_w",
        "patient_h",
        "diagnosis",
        "operation",
        "anest",
        "regisby",
        "createdAt",
        "online_case"
    ]
}

schema1 = {
    "type": "object",
    "properties": {
        "end":          { "type": "string", "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$" },
        "online_case":  { "type": "string", "pattern": "^[a-zA-Zก-๙]"},
    },
    "required":[
        "end",
        "online_case"
    ]

}
